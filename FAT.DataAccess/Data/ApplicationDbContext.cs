﻿using System;
using System.Collections.Generic;
using System.Text;
using FAT.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FAT.DataAccess.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Services> Services { get; set; }
        public DbSet<Queue> Queue { get; set; }
        public DbSet<Turn> Turn { get; set; }
        public DbSet<DataTurn> DataTurn { get; set; }

        
    }
}
