﻿using FAT.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace FAT.DataAccess.Data.Repository
{
   public interface IQueueRepository : IRepository<Queue>
    {
        IEnumerable<SelectListItem> GetListQueues();

        void Update(Queue queue);
    }
}
