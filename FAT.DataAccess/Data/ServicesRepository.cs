﻿using FAT.DataAccess.Data.Repository;
using FAT.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FAT.DataAccess.Data
{
    public class ServicesRepository : Repository<Services>, IServiceRepository
    {
        private readonly ApplicationDbContext _db;

        public ServicesRepository(ApplicationDbContext db) : base (db)
        {
            _db = db;
        }

        public IEnumerable<SelectListItem> GetListServices()
        {
            return _db.Services.Select(i => new SelectListItem()
            {
                Text = i.header,
                Value = i.id.ToString()
            });
        }

        public void Update(Services services)
        {
            var objDb = _db.Services.FirstOrDefault(s => s.id == services.id);
            objDb.header = services.header;
            objDb.tittle = services.tittle;
            objDb.description = services.description;

            _db.SaveChanges();
        }
    }
}
