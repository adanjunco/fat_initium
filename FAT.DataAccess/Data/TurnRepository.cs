﻿using FAT.DataAccess.Data.Repository;
using FAT.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FAT.DataAccess.Data
{
    public class TurnRepository : Repository<Turn>, ITurnRepository
    {
        private readonly ApplicationDbContext _db;

        public TurnRepository(ApplicationDbContext db) : base (db)
        {
            _db = db;
        }

        

        public void Select(Turn turn)
        {
            //var ObjDb = _db.Turn.Select()
        }

        public void Update(Turn turn)
        {
            var objDb = _db.Turn.FirstOrDefault(s => s.Id == turn.Id);
            objDb.Status = turn.Status;
            

            //_db.SaveChanges();
        }

        
    }
}
