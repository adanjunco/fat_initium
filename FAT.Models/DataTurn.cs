﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FAT.Models
{
    public class DataTurn
    {
        [Key]
        public int id { get; set; }
        public int id_turn { get; set; }
        public int id_queue { get; set; }
        public int position { get; set; }
        public DateTime process_time { get; set; }
    }
}


