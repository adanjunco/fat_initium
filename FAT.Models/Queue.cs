﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FAT.Models
{
    public class Queue
    {
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage = "Es necesario un nombre para la cola de turnos")]
        [Display(Name = "Cola")]
        public string name_queue { get; set; }

        [Required(ErrorMessage = "Es necesario asignar un tiempo a la cola de turnos")]
        public int time { get; set; }
    }
}
