﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FAT.Models
{
    public class Turn
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "ID del cliente")]
        public int Cliid { get; set; }

        [Required(ErrorMessage = "El nombre del cliente es necesario")]
        [Display(Name = "Nombre del cliente")]
        [StringLength(25)]
        public string CliName { get; set; }

        public int Status { get; set; }

        [Required]
        public int ServicesId { get; set; }

        [ForeignKey("ServicesId")]

        public Services Services { get; set; }

        [Required]
        public int QueueId { get; set; }

        [ForeignKey("QueueId")]

        public Queue Queue { get; set; }
        public DataTurn DataTurn { get; set; }




    }
}
