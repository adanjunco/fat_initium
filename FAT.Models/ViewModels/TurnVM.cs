﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace FAT.Models.ViewModels
{
    public class TurnVM
    {
        public Turn Turn { get; set; }

        public IEnumerable<SelectListItem> ListServices { get; set; }

        public IEnumerable<SelectListItem> ListQueues { get; set; }
    }
}
